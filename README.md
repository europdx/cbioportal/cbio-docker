# Cbio-docker-compose

## Requirements
 * docker
 * docker-compose



## Deployment

1. Clone the repository
2. Copy sql ini files to mysql/docker-entrypoint-initdb.d
3. Change variable HOST in .env_file
4. Copy SSL key and crt to proxy/apache.key and proxy/apache.crt
5. Build images with <code>docker-compose build</code>
6. Launch <code>docker-compose up -d</code>

Cbioportal should be available at

<code>https://HOST</code>

<code>http://HOST</code>

<code>http://HOST:8080</code> - bypassing proxy

Logs can be checked with

<code>docker-compose logs</code>

Importing data

Copy data to folder /cbio/data-import . Currently used data are available at https://gitlab.ics.muni.cz/europdx-public/europdx-data-public .

Import data with following commands

<code>docker exec cbio metaImport.py -u http://cbio:8080/cbioportal -s /data-import/UNITO_dataset_20170626_curated/ -o
docker exec cbio metaImport.py -u http://cbio:8080/cbioportal -s /data-import/VHIO_dataset_20160726_curated/ -o</code>
